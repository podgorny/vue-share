import Vue from "vue";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
import colors from "vuetify/lib/util/colors";

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: "md"
  },
  theme: {
    themes: {
      light: {
        primary: colors.indigo.lighten1,
        accent: colors.amber.accent3,
        secondary: colors.blue.accent1,
        success: colors.green.lighten2,
        info: colors.blueGrey.lighten2,
        warning: colors.orange.lighten2,
        error: colors.red.lighten2
      }
    }
  }
});
